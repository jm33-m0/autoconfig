#!/bin/bash

# by: jm33-ng
# 2017-06-10
# change log
# 2018-03-24 - switch to spacevim
# 2018-08-26 - using Arch again
# 2019-04-23 - config archcn repo, ufw, virt-mgr, google drive cron job, fontconfig,
#              wireguard service udp2raw service, environment

# TODO:
# - virt-manager
# - drive sync cron job
# - fontconfig fallbacks

# install curl if no curl is detected
pacman -Syyu
pacman -S curl wget

function check_root() {
    if [ "$(id -u)" -ne 0 ]; then
        echo '[-] Please run me as root'
        exit 1
    fi
}

# install sshd_config
function sshd_config() {
    check_root
    echo '[*] Installing ssh_config'
    cp -av ./config/sshd_config /etc/ssh/sshd_config
    systemctl restart ssh.service || service sshd restart

    echo '[*] Adding user jm33'
    test -e "$HOME/.ssh" || mkdir -p "$HOME/.ssh"
    cat <<EOF >"$HOME/.ssh/authorized_keys"
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDVaXjv5cX1pjgURLBSleYZYK/jQNr+RF1Sdqa9RHQTGaBynfuuESMU/0BGu8BQLo83F4z5MEzmpG8zOkKjJzbE/1cqfgWEK2KOX5O1LlwqbfMg+8IJd0LMhdlY1o7mvV59s6ke9qcosyq3W3aoRXultfBRgE9GrPxyYmuJyQTiM3S6Y0PDC6FvhyfXpj0lKc6J6vJMF8pzEeWbrqdcd/bBAdpp9wonEs3NBFRso4EwrUV+j9nP61/bD1QfkyjGnJxD8ufcE5V0dLS1SLkGW5ilgp/5fagu9Wj9pEHLNTuUO1pl9tbTEFSXvkeNBCrqBCYH7v4fQRlTtI+P/vzWRiuJLabcvJcwBbaJkc9UoqJ6omw0gBonlY4f5PLHA0epx42cCIcU+7TUAqn2gpqQ9ZCFBYV01e4e+uaC2ko1Es6ua8jrIc2OHuaW8n2MsHfbQfuknDg/08fJu6O7We33pzE2Xc4anOivVfM4MSN5U8BJH5AOIhJA84QrH8PSjIalEowqZDXxb+LVkIPhG7wlqr8kw7wq2ZK9WMz+5tT3XBcJYlPWqwwy/5gKjj9JLBwTiDw1NtXM04327ygKaPBKALfSXSXTB7B1rars7KclgKZ1u3elghqy3jB3vVBCStrxue8aiP7EQhDgfBWZLNhoAFURzfGQ5VDSUHtH+L6K8Ha2tw== jm33@jm33.me
EOF
    useradd -u 1001 -m jm33 &&
        mkdir /home/jm33/.ssh
    cp ~/.ssh/authorized_keys /home/jm33/.ssh
}

function vim_install() {
    echo '[*] Installing Vim'
    curl -sLf https://github.com/jm33-m0/vim_dotfiles/raw/master/install.sh | bash
}

function zsh_install() {
    echo '[*] Installing oh-my-zsh'
    bash ./oh-my-zsh.sh
}

function install_packages() {
    check_root
    mkdir -p /projects/golang

    echo '[*] Installing daily packages'
    pacman -S linux-headers tmux nmap htop iftop mtr powerline powerline-fonts awesome-terminal-fonts go git zsh make cmake clang autoconf automake wireguard-tools wireguard-dkms ufw ufw-extras ttf-ubuntu-font-family ttf-monaco adobe-source-han-sans-cn-fonts adobe-source-code-pro-fonts adobe-source-sans-pro-fonts adobe-source-serif-pro-fonts universal-ctags-git papirus-icon-theme freerdp dnsmasq virt-manager qemu whois udp2raw-tunnel ipset wireshark-cli wireshark-qt neofetch shfmt tidy js-beautify nodejs npm kcm-fcitx xsel xclip ttf-font-awesome xorg-xinput ksshaskpass openssh noto-fonts noto-fonts-emoji noto-fonts-extra fcitx fcitx-qt4 fcitx-gtk2 fcitx-gtk3 fcitx-rime fcitx-ui-light fcitx-configtool telegram-desktop
}

# add archlinuxcn repo
# shellcheck disable=SC2016
if ! grep -i 'archlinuxcn' /etc/pacman.conf >/dev/null 2>&1; then
    pacman -S archlinuxcn-keyring
    echo '
[archlinuxcn]
Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch' >>/etc/pacman.conf
fi
install_packages

test -e ~/.vim || vim_install
test -e ~/.oh-my-zsh || zsh_install
grep "45672" /etc/ssh/sshd_config || sshd_config

chown -R jm33:jm33 /home/jm33/.*
chown -R jm33:jm33 /projects/golang

echo "[*] Configuring tmux..."
cp ./config/tmux.conf ~/.tmux.conf

echo "[*] Enabling BBR..."
if ! grep -i "bbr" /etc/sysctl.conf >/dev/null 2>&1; then
    echo "net.core.default_qdisc=fq" >>/etc/sysctl.conf
    echo "net.ipv4.tcp_congestion_control=bbr" >>/etc/sysctl.conf
    sysctl -p
fi
echo "[*] Checking BBR..."
sysctl net.ipv4.tcp_available_congestion_control
sysctl net.ipv4.tcp_congestion_control
lsmod | grep bbr

echo "[*] Setting up wireguard and udp2raw services"
cp -av ./services/* /etc/systemd/system
systemctl enable wg@wg0.service
systemctl start wg@wg0.service
systemctl enable udp2raw.service
systemctl start udp2raw.service

echo "[*] Enabling UFW"
ufw enable

echo "[*] Environment variables"
cp -av ./config/environment /etc/environment

echo "[*] Font fallback"
cp -av ./config/fonts.conf ~/.config/fontconfig/fonts.conf
