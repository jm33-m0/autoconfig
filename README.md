# autoconfig

> **DO NOT install this on your system, since I wrote this only for myself...**

### Install oh-my-zsh

```bash
wget 'https://gitlab.com/jm33-m0/autoconfig/raw/master/oh-my-zsh.sh' -O - | bash
```

### Initialize Arch

```bash
wget 'https://gitlab.com/jm33-m0/autoconfig/raw/master/install.sh' -O - | bash
```
